﻿using Dapper;
using Microsoft.Build.Framework;
using NuGet.Protocol;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Data;
using Microsoft.Data.SqlClient;
using WebProject.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace WebProject.DAL
{
    public class DbHelper:IDbHelper
    {
        private string conStr;
        private readonly IConfiguration _configuration;
        private readonly ILogger<DbHelper> logger;

        public DbHelper(IConfiguration config, ILogger<DbHelper> _logger)
        {
            _configuration = config;
            conStr = _configuration.GetConnectionString("Localhost");
            logger = _logger;
        }

        public async Task<T> GetSingleAsync<T>(string command, object param)
        {
            T enity;
            using (SqlConnection con = new SqlConnection(conStr))
            {
                try
                {
                    enity = (await con.QueryAsync<T>(command, param)).FirstOrDefault();
                }
                catch (Exception ex) {
                    logger.LogError($"Error:{ex.Message}");
                    return default(T);
                }
            }
            return enity;
        }

        public async Task<List<T>> GetAllAsync<T>(string command, object param)
        {
            using (SqlConnection con = new SqlConnection(conStr))
            {
                return (await con.QueryAsync<T>(command, param)).ToList();
            }
        }

        public async Task<string> ExecuteAsync(string command, object param)
        {
            string result = "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                await con.OpenAsync();
                using (SqlTransaction tran = con.BeginTransaction())
                {
                    try
                    {
                        await con.ExecuteAsync(command, param, transaction: tran, commandTimeout: 60, commandType: CommandType.Text);
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        logger.LogError($"Error:{ex.Message}");
                        return ex.Message;
                    }
                }
            }
            return "" ;
        }

        public async Task<string> ExecuteMutiAsync<T>(string command, List<T> param)
        {
            string result = "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                await con.OpenAsync();
                using (SqlTransaction tran = con.BeginTransaction())
                {
                    try
                    {
                        await con.ExecuteAsync(command, param, transaction: tran, commandTimeout: 60, commandType: CommandType.Text);
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        logger.LogError($"Error:{ex.Message}");
                        return ex.Message;
                    }
                }
            }
            return result;
        }
    }
}
