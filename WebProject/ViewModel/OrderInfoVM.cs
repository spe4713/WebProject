﻿using WebProject.Models;

namespace WebProject.ViewModel
{
    public class OrderInfoVM
    {
        public string OrderId { get;set; }
        public string CustomerID { get; set; }
        public string EmployeeID { get; set; }  
        public string? LastName { get; set; }   
        public string? FirstName { get; set; }  
        public DateTime? OrderDate { get; set; } 
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get;set; }   
        public int? ShipVia { get; set; }    
        public int? Freight { get; set; }    
        public string? ShipName { get; set; } 
        public string? ShipAddress { get; set; } 
        public string? ShipCity { get; set; }    
        public string? ShipRegion { get; set; }  
        public string? ShipPostalCode { get; set;}
        public string? ShipCountry { get; set; } 
        public List<OrderDetailVM> OrderDetailInfos { get; set; }  
    }
}
