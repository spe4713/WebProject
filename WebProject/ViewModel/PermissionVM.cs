﻿using Microsoft.AspNetCore.Mvc.Rendering;
using WebProject.Models;

namespace WebProject.ViewModel
{
    public class PermissionVM
    {
        public string SystemId { get; set; }  
        public string SystemName { get; set; }  
        public List<SelectListItem> PermissionList { get; set; }
    }
}
