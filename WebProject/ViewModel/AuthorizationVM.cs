﻿namespace WebProject.ViewModel
{
    public class AuthorizationVM
    {
        public string Role { get; set; }
        public string SystemTag { get; set; }   
        public string Url { get; set; }
    }
}
