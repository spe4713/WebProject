﻿namespace WebProject.ViewModel
{
    public class FoodMapVM
    {
        public string Id { get;set; }
        public string Name { get;set; }
        public string Description { get;set; }
        public string Add { get;set; }
        public string ZipCode { get;set; }  
        public string Region { get;set; }
        public string Town { get;set; }
        public string Tel { get;set; }
        public string Opentime { get;set; }
        public string Website { get;set; }
        public string Picture1 { get;set; }
        public string Picdescribe1 { get; set; }
        public string Picture2 { get; set; }
        public string Picdescribe2 { get; set; }    
        public string Picture3 { get;set; }
        public string Picdescribe3 { get; set; }
        public string Px { get; set; }
        public string Py { get; set; }
        public string Class { get; set; }
        public string Map { get; set; }
        public string Parkinginfo { get; set;}
    }
}
