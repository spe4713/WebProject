﻿namespace WebProject.ViewModel
{
    public class OrderVM
    {
        public int OrderID { get; set;}
        public DateTime? OrderDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public string? ShipName { get; set;}
        public string? ShipCity { get; set; }
        public string? ShipPostalCode { get; set; }
        public string? ShipCountry { get; set; }
        public decimal Freight { get; set; }
        public string? CustomerID { get;set;}
        public string? CompanyName { get; set; }
        public string? ContactName { get; set; }
        public string? ContactTitle { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? Country { get;set; }
        public string? PostalCode { get; set; }
        public string? Phone { get; set; }   
        public string? Fax { get; set; } 
        public List<OrderDetailVM> OrderDetail { get; set; }   
    }
}
