﻿namespace WebProject.ViewModel
{
    public class SpiderVM
    {
        public string Title { get;set; }
        public string Url { get; set; }
        public string PicUrl { get; set; }
    }
}
