﻿namespace WebProject.ViewModel
{
    public class OrderDetailVM
    {
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
        public string? Region { get; set; }
        public string ProductName { get; set; }
        public double ProductUnitPrice { get; set; }
        public string QuantityPerUnit { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitsOnOrder { get; set; }
        public int ReorderLevel { get; set; }
        public int Discontinued { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get;set; }
        public string Address { get; set; }     
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string? Fax { get; set; }
        public string? HomePage { get; set;}
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }   
    }
}
