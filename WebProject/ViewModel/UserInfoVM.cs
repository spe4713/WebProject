﻿namespace WebProject.ViewModel
{
    public class UserInfoVM
    {
        public string Email { get; set; }
        public string Name { get; set; }    
        public DateTime? Birthday { get; set; }
        public List<AuthorizationVM> allowSystem { get; set; }   
    }
}
