﻿using System.Linq.Expressions;
using WebProject.DAL_EF;
using WebProject.Models;

namespace WebProject.BLL
{
    public class RegionService
    {
        private readonly RegionRepository RegionRepository;
        public RegionService(RegionRepository _RegionRepository)
        {
            RegionRepository = _RegionRepository;
        }
        public IQueryable<Region> GetRegion(Expression<Func<Region, bool>> funcWhere, NorthWindContext Context)
        {
            if (Context != null) RegionRepository.SetDbContextForRelation(Context);
            if (funcWhere != null) return RegionRepository.GetRegion(funcWhere);

            return RegionRepository.GetRegion(o => true);
        }
    }
}
