﻿using Microsoft.AspNetCore.JsonPatch.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Identity.Client;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using WebProject.DAL_EF;
using WebProject.Models;
using WebProject.ViewModel;

namespace WebProject.BLL
{
    public class EmployeeService
    {
        private readonly EmployeeRepository EmployeeRepository;
        private readonly RegionService RegionService;
        private readonly TerritoriesService TerritoriesService;
        private readonly EmployeeTerritoriesRepository EmployeeTerritoriesRepository;
        private readonly NorthWindContext NorthWindContext;
        public EmployeeService(EmployeeRepository _employeeRepository,RegionService _regionService,TerritoriesService _territoriesService,EmployeeTerritoriesRepository _employeeTerritories,NorthWindContext northwindContext)
        {
            EmployeeRepository = _employeeRepository;
            RegionService = _regionService;
            TerritoriesService = _territoriesService;
            EmployeeTerritoriesRepository = _employeeTerritories;
            NorthWindContext = northwindContext;
        }

        public async Task<List<EmpInfoVM>> GetEmployeeInfo(int Id)
        {
            List<EmpInfoVM> empList=new List<EmpInfoVM>();
            EmployeeTerritoriesRepository.SetDbContextForRelation(NorthWindContext);
            var empInfo =await EmployeeRepository.GetEmployee(m => true)
                         .Join(EmployeeTerritoriesRepository.GetEmployeeTerritories(n => true), m => m.EmployeeId, n => n.EmployeeId, (m, n) => new { m, n })
                         .Join(TerritoriesService.GetTerritories(c => true, NorthWindContext), c => c.n.TerritoryId, s => s.TerritoryId, (c, s) => new { c, s })
                         .Join(RegionService.GetRegion(o => o.RegionId==Id, NorthWindContext), o => o.s.RegionId, p => p.RegionId, (o, p) => new { o, p })
                         .Select(a => new EmployeeVM()
                         {
                             ID=a.o.c.m.EmployeeId,
                             LastName = a.o.c.m.LastName,
                             FirstName = a.o.c.m.FirstName,
                             Title = a.o.c.m.Title,
                             TitleOfCourtesy = a.o.c.m.TitleOfCourtesy,
                             BirthDate = a.o.c.m.BirthDate,
                             HireDate = a.o.c.m.HireDate,
                             Address = a.o.c.m.Address,
                             City = a.o.c.m.City,
                             Region = a.o.c.m.Region,
                             PostalCode = a.o.c.m.PostalCode,
                             Country = a.o.c.m.Country,
                             HomePhone = a.o.c.m.HomePhone,
                             Notes = a.o.c.m.Notes,
                             ReportsTo = a.o.c.m.ReportsTo,
                             PhotoPath = a.o.c.m.PhotoPath,
                             TerritoryDescription = a.o.s.TerritoryDescription,   //負責業務區域
                             RegionDescription = a.p.RegionDescription,  //負責業務方位，比對資料一位業務不會有負責兩個方位
                         }).ToListAsync();

            var empId = empInfo.Select(m => m.ID).Distinct().ToArray();

            var regionInfo = await RegionService.GetRegion(m => true, NorthWindContext)
                                  .Join(TerritoriesService.GetTerritories(n => true, NorthWindContext),m=>m.RegionId,n=>n.RegionId, (m, n) => new
                                  {
                                      m,n
                                  }).ToListAsync();

            foreach(var id in empId)
            {
                List<SelectListItem> TerritoryList = new List<SelectListItem>();
                var emp = empInfo.Where(m => m.ID == id).FirstOrDefault();
                var tempTerritoryDescription = empInfo.Where(m=>m.ID == id).Select(m => m.TerritoryDescription).Distinct().ToArray();
                var tempRegionDescript = regionInfo.Where(s => s.m.RegionDescription == emp.RegionDescription).ToList();
                foreach(var item in tempRegionDescript)
                {
                    bool selected = false;
                    if (tempTerritoryDescription.Contains(item.n.TerritoryDescription)) selected = true;

                    TerritoryList.Add(new SelectListItem()
                    {
                        Text=item.n.TerritoryDescription,
                        Value=item.n.TerritoryId,
                        Selected=selected
                    });
                }

                empList.Add(new EmpInfoVM()
                {
                    IsEdit=false,
                    ID = emp.ID,
                    LastName = emp.LastName,
                    FirstName = emp.FirstName,
                    Title = emp.Title,
                    TitleOfCourtesy = emp.TitleOfCourtesy,
                    BirthDate = emp.BirthDate,
                    HireDate = emp.HireDate,
                    Address = emp.Address,
                    City = emp.City,
                    Region = emp.Region,
                    PostalCode = emp.PostalCode,
                    Country = emp.Country,
                    HomePhone = emp.HomePhone,
                    Notes = emp.Notes,
                    ReportsTo = emp.ReportsTo,
                    PhotoPath = emp.PhotoPath,
                    TerritoryDescription = TerritoryList,
                    RegionDescription = emp.RegionDescription,
                });
            }
            return empList;
        }

        public async Task<List<SelectListItem>> GetDplRegion()
        {
                List<SelectListItem> dplRegion = await RegionService.GetRegion(m => true, NorthWindContext).Select(m => new SelectListItem()
                {
                    Text = m.RegionDescription,
                    Value = m.RegionId.ToString(),
                }).OrderBy(m => m.Value).ToListAsync();
            return dplRegion;
        }
    }
}
