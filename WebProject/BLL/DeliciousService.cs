﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection.Emit;
using System.Security.Claims;
using System.Xml;
using System.Xml.Linq;
using WebProject.ViewModel;


namespace WebProject.BLL
{
    public class DeliciousService
    {
        private readonly IHttpClientFactory httpClient;
        public DeliciousService(IHttpClientFactory _httpClient)
        {

            httpClient = _httpClient;
        }


        public async Task<List<FoodMapVM>> GetTaiwanDelicios()
        {
            List<FoodMapVM> FoodInfo = new List<FoodMapVM>();
            string url = "https://media.taiwan.net.tw/XMLReleaseALL_public/restaurant_C_f.json";
            var client = httpClient.CreateClient();
            var ResponseMessage = await client.GetAsync(url);
            if (ResponseMessage.IsSuccessStatusCode)
            {
                string json = await ResponseMessage.Content.ReadAsStringAsync();
                JObject jsonObject = JObject.Parse(json);
                if (jsonObject == null) return null;
                JArray infoAry = (JArray)jsonObject["XML_Head"]["Infos"]["Info"];
                foreach (var item in infoAry)
                {
                    FoodInfo.Add(new FoodMapVM()
                    {
                        Id = item["Id"]?.ToString() ?? "",
                        Name = item["Name"]?.ToString() ?? "",
                        Description = item["Description"]?.ToString() ?? "",
                        Add = item["Add"]?.ToString() ?? "",
                        ZipCode = item["ZipCode"]?.ToString() ?? "",
                        Region = item["Region"]?.ToString() ?? "",
                        Town = item["Town"]?.ToString() ?? "",
                        Tel = item["Tel"]?.ToString() ?? "",
                        Opentime = item["Opentime"]?.ToString() ?? "",
                        Website = item["Website"]?.ToString() ?? "",
                        Picture1 = item["Picture1"]?.ToString() ?? "",
                        Picdescribe1 = item["Picdescribe1"]?.ToString() ?? "",
                        Picture2 = item["Picture2"]?.ToString() ?? "",
                        Picdescribe2 = item["Picdescribe2"]?.ToString() ?? "",
                        Picture3 = item["Picture3"]?.ToString() ?? "",
                        Picdescribe3 = item["Picdescribe3"]?.ToString() ?? "",
                        Px = item["Px"]?.ToString() ?? "",
                        Py = item["Py"]?.ToString() ?? "",
                        Class = item["Class"]?.ToString() ?? "",
                        Map = item["Map"]?.ToString() ?? "",
                        Parkinginfo = item["Parkinginfo"]?.ToString() ?? "",
                    });
                }
            }
            return FoodInfo;
        }

        public async Task<List<string>> GetTaiwanCity()
        {
            List<string> location = new List<string>();
            string url = "https://api.nlsc.gov.tw/other/ListCounty";
            var client = httpClient.CreateClient();
            var ResponseMessage = await client.GetAsync(url);
            if (ResponseMessage.IsSuccessStatusCode)
            {
                string info = await ResponseMessage.Content.ReadAsStringAsync();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(info);
                XmlNodeList xml = doc.SelectNodes("countyItems/countyItem");
                foreach (XmlNode item in xml)
                {
                    location.Add(item.SelectSingleNode("countyname").InnerText);
                }
                return location;
            }
            return null;
        }
    }
}
