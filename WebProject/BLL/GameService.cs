﻿using NuGet.Protocol.Plugins;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using System;
using WebProject.ViewModel;

namespace WebProject.BLL
{
    public class GameService
    {
        private readonly IWebHostEnvironment environment;
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory httpClient;
        public GameService(IWebHostEnvironment _environment, IConfiguration _configuration, IHttpClientFactory _httpClient)
        {
            environment = _environment;
            configuration = _configuration;
            httpClient = _httpClient;
        }

        public async Task<List<SpiderVM>> GetSpider(ChromeDriver driver)
        {
            List<SpiderVM> spider = new List<SpiderVM>();
            try
            {
                string url = "http://www.eyny.com/";
                driver.Navigate().GoToUrl(url);
                driver.FindElement(By.XPath("//*[@id=\"category_1790\"]/ul/li[25]/a")).Click();
                var divElement = driver.FindElement(By.ClassName("pg"));
                var PageAry = divElement.FindElements(By.TagName("a"));
                List<PageVM> PageList = new List<PageVM>();
                foreach (var tag in PageAry)
                {
                    var page = new PageVM
                    {
                        href = tag.GetAttribute("href"),
                        pageNum = tag.Text,
                    };
                    PageList.Add(page);
                }
                PageList = PageList.Count > 7 ? PageList.Take(10).ToList() : PageList;
                for (var i = 0; i < PageList.Count; i++)
                {
                    if (i > 0)
                    {
                        driver.Navigate().GoToUrl($"{PageList[i].href}");
                    }
                    IWebElement element = driver.FindElement(By.XPath("//*[@id=\"wp\"]/table[4]")); 
                    List<IWebElement> lstTrElement = new List<IWebElement>(element.FindElements(By.TagName("tr")));
                    foreach (var tr in lstTrElement)
                    {
                        List<IWebElement> tdList = new List<IWebElement>(tr.FindElements(By.TagName("td")));
                        if (tdList.Count > 0)
                        {
                            foreach (var cell in tdList)
                            {
                                var tempObj = new SpiderVM();
                                var link = cell.FindElement(By.TagName("a"));
                                tempObj.Url = link.GetAttribute("href");
                                tempObj.Title = link.GetAttribute("title");
                                var img = cell.FindElement(By.TagName("img"));
                                string PicUrl = img.GetAttribute("src");
                                await DownloadImage(PicUrl);
                                tempObj.PicUrl = $"{Path.Combine(configuration["AppSetting:WebDomin"], configuration["AppSetting:ImageFile"], Path.GetFileName(PicUrl))}";
                                spider.Add(tempObj);
                            }
                        }
                    }
                }
                #region[查詢後抓取資料，避免造成對方server loading太重，註解掉這段]
                //driver.FindElement(By.XPath("//*[@id=\"scbar_txt\"]")).SendKeys($"泳裝");
                //driver.FindElement(By.XPath("//*[@id=\"scbar_btn\"]/strong")).Click();
                //IWebElement table = driver.FindElement(By.XPath("//*[@id=\"threadlist\"]/div/table"));
                //var tbodyElement = new List<IWebElement>(table.FindElements(By.TagName("tr")));
                //for (int j = 0; j < tbodyElement.Count; j++)
                //{
                //    if (j > 0)
                //    { 
                //        var tempObj=new SpiderVM();
                //        try
                //        {
                //            var imgElement = tbodyElement[j].FindElement(By.XPath(".//img[@class='p_pre_none']"));
                //            tempObj.PicUrl = imgElement.GetAttribute("src");
                //        }
                //        catch (Exception ex)
                //        {
                //            tempObj.PicUrl = "";
                //        }

                //        try
                //        {
                //            var imgTextElement = tbodyElement[j].FindElement(By.XPath(".//th[@class='common']/a"));
                //            tempObj.Title = imgTextElement.Text;
                //            tempObj.Url = imgTextElement.GetAttribute("href");
                //        }
                //        catch(Exception ex)
                //        {
                //            var imgTextElement = tbodyElement[j].FindElement(By.XPath(".//th[@class='lock']/a"));
                //            tempObj.Title = imgTextElement.Text;
                //            tempObj.Url = imgTextElement.GetAttribute("href");
                //        }
                //        swimSuit.Add(tempObj);
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                driver.Quit();
            }
            return spider;
        }

        public async Task DownloadImage(string imageUrl)
        {
            var client = httpClient.CreateClient();
            using (var response = await client.GetAsync(imageUrl.Replace(":8000","")))
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string fileName = Path.GetFileName(imageUrl);
                    string filePath = Path.Combine(environment.ContentRootPath, configuration["AppSetting:ImageFile"], fileName);
                    var stream = await response.Content.ReadAsStreamAsync();
                    using (var fileStram = new FileStream(filePath, FileMode.Create))
                    {
                        await stream.CopyToAsync(fileStram);
                    }
                }
            }
        }
    }
}
