﻿using Dapper;
using Microsoft.Build.Framework;
using OpenQA.Selenium.Interactions;
using System.Data.SqlClient;
using WebProject.Common;
using WebProject.InputModel;
using WebProject.Interface;
using WebProject.Models;
using WebProject.ViewModel;

namespace WebProject.BLL
{
    public class UserService
    {
        private readonly IConfiguration configuration;
        private readonly EncryptService encryptHelper;
        private readonly IDbHelper helper;
        public UserService(IConfiguration _configuration, EncryptService _encryptHelper, IDbHelper _helper)
        {
            configuration = _configuration;
            encryptHelper = _encryptHelper;
            helper = _helper;
        }

        public async Task<List<UserInfoVM>> GetUserInfo()
        {
            List<UserInfoVM> userList = (await helper.GetAllAsync<UserInfoVM>("select Email,Name from UserInfo", null)).ToList();
            return userList;
        }

        public async Task<UserInfoVM> UserLogin(LoginIM user)
        {
            var userInfo = (await helper.GetSingleAsync<UserInfoVM>("select * from UserInfo where Email=@Email and Password=@Password", new { Email = user.UserEmail, Password = encryptHelper.EncryptSha512(user.Password) }));
            if (userInfo != null)
            {
                await helper.ExecuteAsync("UPDATE UserInfo SET LoginDate = GETDATE() where Email=@Email", new { userInfo.Email });
                return userInfo;
            }
            return null;
        }

        public async Task<string> CreateUser(string Name, string Email, string Password, DateTime Birthday)
        {

            var user = (await helper.GetSingleAsync<UserInfoVM>("SELECT * FROM UserInfo WHERE Email=@Email", new { Email }));
            if (user == null)
            {

                return await helper.ExecuteAsync("INSERT INTO UserInfo(Email,Password,Name,Birthday,CreateDate) VALUES (@Email,@Password,@Name,@Birthday,GETDATE())"
                                               , new { Email, Password = encryptHelper.EncryptSha512(Password), Name, Birthday });


            }
            else return "已有此使用者";
        }

        public async Task<string> UpdateUserPwd(string Email, string NewPassword, string OldPassword)
        {
            string result = "";

            UserInfo user = (await helper.GetSingleAsync<UserInfo>("SELECT * FROM UserInfo WHERE Email=@Email AND Password=@Password", new { Email, Password = encryptHelper.EncryptSha512(OldPassword) }));
            if (user != null)
            {
                return await helper.ExecuteAsync("UPDATE UserInfo SET Password=@Password,UpdateDate=GETDATE() where Email=@Email", new { Password = encryptHelper.EncryptSha512(NewPassword), Email });
            }
            else return "密碼輸入錯誤，修改失敗";

        }
    }
}
