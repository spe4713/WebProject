﻿using System.Linq.Expressions;
using WebProject.DAL_EF;
using WebProject.InputModel;
using WebProject.Models;

namespace WebProject.BLL
{
    public class TerritoriesService
    {
        private readonly TerritoriesRepository territoriesRepository;
        private readonly EmployeeTerritoriesRepository employeeTerritoriesRepository;
        public TerritoriesService(TerritoriesRepository _territoriesRepository,EmployeeTerritoriesRepository _employeeTerritoriesRepository)
        {
            territoriesRepository = _territoriesRepository;
            employeeTerritoriesRepository = _employeeTerritoriesRepository;
        }

        public IQueryable<Territories> GetTerritories(Expression<Func<Territories, bool>> funcWhere, NorthWindContext Context)
        {
            if (Context != null) territoriesRepository.SetDbContextForRelation(Context);
            if (funcWhere != null) return territoriesRepository.GetTerritories(funcWhere);

            return territoriesRepository.GetTerritories(o => true);
        }

        public async Task<string> UpdateTerritory(TerritoryIM employeeInfo)
        {
            var result = await employeeTerritoriesRepository.GetEmployeeTerritories(m => m.EmployeeId == employeeInfo.EmployeeID).ToListAsync();
            string msg = "";
            if (result.Count > 0) msg = employeeTerritoriesRepository.BatchDeleteEmployeeTerritories(result);
            if (string.IsNullOrEmpty(msg))
            {
                List<EmployeeTerritories> territoryList = new List<EmployeeTerritories>();
                foreach (var id in employeeInfo.TerritoryIds)
                {
                    territoryList.Add(new EmployeeTerritories()
                    {
                        EmployeeId = employeeInfo.EmployeeID,
                        TerritoryId = id,
                    });
                }
                 msg = employeeTerritoriesRepository.BatchCreateEmployeeTerritories(territoryList);
            }
            return msg;
        }
    }
}
