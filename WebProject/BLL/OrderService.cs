﻿using WebProject.ViewModel;
using Dapper;
using Microsoft.Data.SqlClient;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using Microsoft.Build.Framework;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using WebProject.Models;
using System.Text;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using System.Net;
using System.Web;
using WebProject.InputModel;
using WebProject.Interface;

namespace WebProject.BLL
{
    public class OrderService
    {
        private readonly IConfiguration config;
        private readonly IDbHelper helper;
        private readonly string ConnStr;
        public OrderService(IConfiguration _config, IDbHelper _helper)
        {
            helper= _helper;
            config = _config;
            ConnStr = config.GetConnectionString("Localhost");
        }


        public async Task<List<OrderVM>> GetOrderBySales(int empId)
        {
            List<OrderVM> orderInfo = (await helper.GetAllAsync<OrderVM>(@"select a.OrderID,a.OrderDate,a.ShippedDate,a.RequiredDate,a.ShipName,a.ShipCity,a.ShipPostalCode,a.ShipCountry,a.Freight,
                                                             ISNULL(a.ShipRegion,'') AS Region,(b.FirstName+' '+b.LastName) AS Name,b.HireDate,b.Country,b.City,b.Address,c.*
                                                             from Orders as a
                                                             inner join Employees as b
                                                             on a.EmployeeID=b.EmployeeID
                                                             inner join Customers as c
                                                             on a.CustomerID=c.CustomerID
                                                             WHERE b.EmployeeID=@paramId", new { paramId = empId }));
            if (orderInfo.Count > 0)
            {
                foreach (var item in orderInfo)
                {

                    item.OrderDetail = (await helper.GetAllAsync<OrderDetailVM>(@"select  a.OrderID,a.UnitPrice,a.Quantity,a.Discount,b.ProductName,b.QuantityPerUnit,b.UnitPrice as ProductUnitPrice,
                                                                                  b.UnitsInStock,b.UnitsOnOrder,b.ReorderLevel,b.Discontinued,c.*,d.*
                                                                                  from [Order Details] as a
                                                                                  inner join Products as b
                                                                                  on a.ProductID=b.ProductID
                                                                                  inner join Suppliers as c
                                                                                  on b.SupplierID=c.SupplierID
                                                                                  inner join Categories as d
                                                                                  on b.CategoryID=d.CategoryID
                                                                                  where OrderID=@OrderId", new { OrderId = item.OrderID }));
                }
            }
            return orderInfo;
        }

        public async Task<List<OrderInfoVM>> GetOrderInfoByEmpId(int empId, string str)
        {
            List<OrderInfoVM> OrderList = new List<OrderInfoVM>();
            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                await con.OpenAsync();
                var param = new DynamicParameters();
                StringBuilder sql = new StringBuilder();
                sql.Length = 0;
                sql.Append(@"select * 
                              from Orders as a
                              inner join Customers as c
                              on a.CustomerID=c.CustomerID
                              inner join Employees as d
                              on a.EmployeeID=d.EmployeeID
                              where a.EmployeeID=@EmpId ");

                param.Add("@EmpId", empId);
                if (!string.IsNullOrEmpty(str))
                {
                    sql.Append(" and (OrderID like @Search OR a.CustomerID like @Search OR a.ShipName like @Search)");
                    param.Add("@Search", $"%{HttpUtility.HtmlEncode(str.Trim())}%");
                }
                OrderList = (await helper.GetAllAsync<OrderInfoVM>(sql.ToString(), param));

                foreach (var order in OrderList)
                {
                    order.OrderDetailInfos = (await helper.GetAllAsync<OrderDetailVM>(@"select  a.OrderID,a.ProductID,a.UnitPrice,a.Quantity,a.Discount,b.ProductName,b.CategoryID,b.QuantityPerUnit,b.UnitPrice as ProductUnitPrice
                                                                                 ,b.UnitsInStock,b.UnitsOnOrder,b.ReorderLevel,b.Discontinued,c.*,d.*
                                                                                 from [Order Details] as a
                                                                                 inner join Products as b
                                                                                 on a.[ProductID]=b.ProductID 
                                                                                 inner join Suppliers as c
                                                                                 on b.SupplierID=c.SupplierID
                                                                                 inner join Categories as d
                                                                                 on b.CategoryID=d.CategoryID
                                                                                 where a.OrderID=@OrderId", new { order.OrderId }));
                }
            }
            return OrderList;
        }

        public async Task<string> UpdateProduct(List<ProductIM> product)
        {
            return await helper.ExecuteMutiAsync<ProductIM>(@"UPDATE [Order Details] SET UnitPrice=@UnitPrice,Quantity=@Quantity,Discount=@Discount WHERE ProductID=@Pid AND OrderID=@OrderId", product);
        }

        public async Task<string> DeleteProcuctInfo(DeleteProductIM product)
        {
            return await helper.ExecuteAsync("DELETE FROM [Order Details] WHERE OrderID=@OrderId AND ProductID=@Pid", new { product.OrderId, product.Pid });
        }
    }
}
