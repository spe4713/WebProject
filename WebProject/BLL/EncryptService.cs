﻿using System.Security.Cryptography;
using System.Text;

namespace WebProject.BLL
{
    public class EncryptService
    {
        public string EncryptSha512(string str)
        {
            SHA512 SHA512 = SHA512.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] hash = SHA512.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }
    }
}
