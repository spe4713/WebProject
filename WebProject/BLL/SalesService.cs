﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;
using WebProject.Interface;

namespace WebProject.BLL
{
    public class SalesService
    {
        private readonly IConfiguration config;
        private readonly string ConnStr;
        private readonly IDbHelper helper;
        public SalesService(IConfiguration _config, IDbHelper _helper)
        {
            config = _config;
            ConnStr = config.GetConnectionString("Localhost");
            helper = _helper;
        }

        public async Task<List<SelectListItem>> GetEmployee()
        {
            return (await helper.GetAllAsync<SelectListItem>(@"select FirstName+' '+LastName as Text,EmployeeID as Value from Employees", null));
        }
    }
}
