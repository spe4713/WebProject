﻿using Dapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OpenQA.Selenium.DevTools.V113.Tracing;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Diagnostics;
using WebProject.Common;
using WebProject.InputModel;
using WebProject.Interface;
using WebProject.Models;
using WebProject.ViewModel;

namespace WebProject.BLL
{
    public class PermissionService
    {
        private readonly IConfiguration config;
        private readonly string ConnStr;
        private readonly UserService userHelper;
        private readonly IDbHelper helper;
        public PermissionService(IConfiguration _config, UserService _userHelper,IDbHelper _helper)
        {
            config = _config;
            ConnStr = config.GetConnectionString("Localhost");
            userHelper = _userHelper;
            helper = _helper;   
        }

        // permission page Index Infomation
        public async Task<List<PermissionVM>> GetPermissionInfo(string email)
        {
            List<PermissionVM> vm = new List<PermissionVM>();
            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                var result = await con.QueryMultipleAsync(@"SELECT A.Email,B.Role,C.SystemName,C.SystemId 
                                                            FROM UserInfo AS A
                                                            INNER JOIN RoleMapping AS B
                                                            ON A.Email=B.Email
                                                            INNER JOIN SystemList AS C
                                                            ON B.SystemId=C.SystemId
                                                            WHERE A.Email=@Email;
                                                            SELECT * FROM SystemList;", new { Email = email });
                var permission = (await result.ReadAsync<UserPermissionVM>()).ToList();
                var systemListInfo = (await result.ReadAsync<SystemList>()).ToList();
                Func<List<SelectListItem>, List<UserPermissionVM>, string, List<SelectListItem>> roleList = GetSelectedRole;
                foreach (var system in systemListInfo)
                {
                    PermissionVM PermissionObj = new PermissionVM
                    {
                        SystemId = system.SystemId,
                        SystemName = system.SystemName,
                        PermissionList = roleList(new Base().RoleType, permission, system.SystemId)
                    };
                    vm.Add(PermissionObj);
                }
            }
            return vm;
        }

        // User Permission Information
        public async Task<List<AuthorizationVM>> GetUserAllowSystem(string email)
        {
            List<AuthorizationVM> allowSystem = (await helper.GetAllAsync<AuthorizationVM>(@"select a.Role,b.SystemTag,b.Url
                                                                       from RoleMapping as a
                                                                       inner join SystemList as b
                                                                       on a.SystemId=b.SystemId
                                                                       where Email=@Email", new { Email = email })).ToList();
            return allowSystem;
        }

        public async Task<string> SetUserAuthority(PermissionIM info, string Operator)
        {
            return (await helper.ExecuteAsync(@"IF EXISTS(SELECT * FROM RoleMapping WHERE Email=@Email AND SystemId=@SystemId)
                                                    BEGIN
                                                       UPDATE RoleMapping SET Role=@Role WHERE Email=@Email AND SystemId=@SystemId;
                                                    END
                                                      ELSE 
                                                    BEGIN
                                                       INSERT INTO RoleMapping(Email,SystemId,Role,CreateDate,Operator) VALUES(@Email,@SystemId,@Role,@CreateDate,@Operator)
                                                    END", new { info.Role, info.SystemId, CreateDate = DateTime.Now, UpdateDate = DateTime.Now, info.Email, Operator }));
        }

        public List<SelectListItem> GetSelectedRole(List<SelectListItem> roleList, List<UserPermissionVM> permissionList, string systemId)
        {
            if (permissionList.Any(m => m.SystemId == systemId))
            {
                var role = permissionList.Where(m => m.SystemId == systemId).FirstOrDefault();
                foreach (var item in roleList)
                {
                    if (item.Value == role.Role)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            return roleList;
        }

        public async Task<string> DelUserAuthority(PermissionIM info)
        {           
            return  await helper.ExecuteAsync("DELETE FROM RoleMapping WHERE Email=@Email AND SystemId=@SystemId", new { info.Email, info.SystemId });
        }
    }
}
