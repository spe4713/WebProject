﻿namespace WebProject.Models
{
    public class UserPermissionVM
    {
        public string Email { get;set; }
        public string Role { get; set; }    
        public string SystemName { get; set; }
        public string SystemId { get;set; }
    }
}
