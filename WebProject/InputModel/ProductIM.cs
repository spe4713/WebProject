﻿namespace WebProject.InputModel
{
    public class ProductIM
    {
        public int OrderId { get; set; }
        public int Pid { get; set; }
        public decimal UnitPrice { get; set; }
        public double Quantity { get; set; }
        public double Discount { get; set; }
    }
}
