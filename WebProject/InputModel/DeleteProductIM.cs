﻿using Microsoft.Data.SqlClient.DataClassification;
using OpenQA.Selenium.DevTools.V113.DOM;

namespace WebProject.InputModel
{
    public class DeleteProductIM
    {
        public int OrderId { get; set; }
        public int Pid { get; set; }
    }
}