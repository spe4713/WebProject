﻿namespace WebProject.InputModel
{
    public class LoginIM
    {
        public string UserEmail { get; set; }
        public string Password { get; set; }
    }
}
