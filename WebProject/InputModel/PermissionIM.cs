﻿namespace WebProject.InputModel
{
    public class PermissionIM
    {
        public string Email { get; set; }   
        public string SystemId { get;set; }
        public string Role { get; set; }
    }
}
