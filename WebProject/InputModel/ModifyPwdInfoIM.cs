﻿namespace WebProject.InputModel
{
    public class ModifyPwdInfoIM
    {
        public string NewPassword { get;set; }  
        public string OldPassword { get;set; }
    }
}
