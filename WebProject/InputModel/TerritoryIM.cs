﻿namespace WebProject.InputModel
{
    public class TerritoryIM
    {
        public int EmployeeID { get; set; }
        public string[] TerritoryIds { get; set; }
    }
}
