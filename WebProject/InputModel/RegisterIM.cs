﻿namespace WebProject.InputModel
{
    public class RegisterIM
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime Birthday { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
