using Microsoft.AspNetCore.ResponseCompression;
using WebProject.BLL;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.FileProviders;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using WebProject.Models;
using System.Text.Json;
using Newtonsoft.Json.Serialization;
using WebProject.DAL_EF;
using WebProject.Interface;
using WebProject.DAL;
using NLog;
using NLog.Web;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();

//回應壓縮
builder.Services.AddResponseCompression(options =>
{
    options.EnableForHttps = true;
    //options.Providers.Add<BrotliCompressionProvider>();
    options.Providers.Add<GzipCompressionProvider>();
    options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "application/json" });
});

//避免ViewBag,ViewData,TempData中文亂碼
builder.Services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All)); 

builder.Services.AddRazorPages().AddRazorRuntimeCompilation();

// Add services to the container.
builder.Services.AddControllersWithViews();


//DI
builder.Services.AddTransient<DeliciousService>();
builder.Services.AddTransient<OrderService>();
builder.Services.AddTransient<SalesService>();
builder.Services.AddTransient<GameService>();
builder.Services.AddTransient<UserService>();
builder.Services.AddTransient<EncryptService>();
builder.Services.AddTransient<PermissionService>();
builder.Services.AddTransient<IDbHelper,DbHelper>(); 

//Service
builder.Services.AddTransient<EmployeeService>();
builder.Services.AddTransient<RegionService>();
builder.Services.AddTransient<TerritoriesService>();

//Repository
builder.Services.AddTransient<EmployeeRepository>();
builder.Services.AddTransient<EmployeeTerritoriesRepository>();
builder.Services.AddTransient<RegionRepository>();
builder.Services.AddTransient<TerritoriesRepository>();


// session and memory chache
builder.Services.AddHttpContextAccessor();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSingleton<IMemoryCache, MemoryCache>();

//NLog
builder.Services.AddLogging();

builder.Services.AddHttpClient();

//ASP.NET Core 中的靜態檔案
builder.Services.AddDirectoryBrowser();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(60);
    //指出用戶端腳本是否無法存取 Cookie
    options.Cookie.HttpOnly = true;
    //指出此 Cookie 是否為應用程式正常運作的必要專案。 如果為 true，則可能會略過同意原則檢查。 預設值為 ， false 但特定元件可能會使用不同的值。
    options.Cookie.IsEssential = true;
    //限制只有在 HTTPS 連線的情況下，才允許使用 Session。如此一來變成加密連線，就不容易被攔截
    options.Cookie.SecurePolicy = CookieSecurePolicy.None;  //iis 上https後，要改屬性 Always
});

builder.Services.AddControllers().AddNewtonsoftJson(option =>
{
    option.SerializerSettings.ContractResolver = new DefaultContractResolver();
}).AddJsonOptions(option =>
{
    option.JsonSerializerOptions.PropertyNamingPolicy = null;
});
builder.Services.AddControllersWithViews().AddNewtonsoftJson();
builder.Services.AddRazorPages().AddNewtonsoftJson();


////Json net 
//builder.Services.AddControllers().AddJsonOptions(option =>
//{
//    //取得或設定值，這個值表示 JSON 是否應該使用美化列印。 根據預設，JSON 會序列化，而不會有任何額外的空白字元
//    option.JsonSerializerOptions.WriteIndented = true;
//    //取得或設定用來指定的值，該值會指定在物件上將屬性的名稱轉換為另一個格式的原則，例如駝峰式大寫   
//    //option.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;   //取得 Camel 命名法大小寫的命名原則。
//    option.JsonSerializerOptions.PropertyNamingPolicy = null;
//    //將一組字元轉換成位元組序列   
//    //https://ithelp.ithome.com.tw/articles/10300655
//    option.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);  //將所有語言都不進行轉換
//});

//NLog
builder.Logging.AddNLog("Nlog.config");

//asp net core identity 
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
.AddCookie(options =>
{
    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
    // FormsAuthentication.SlidingExpiration 的設定，而且預設是 True，只要使用者有持續使用系統，他就會自動重新設定票證的失效時間
    options.SlidingExpiration = true;
    options.LoginPath = "/Home/Index";
    options.LogoutPath = "/Home/Logout";
});

//EF Core connection 
builder.Services.AddDbContext<NorthWindContext>(options => options.UseSqlServer("Name=Localhost"));

//use cros
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "MyAllowSpecificOrigins",
                      policy =>
                      {
                          policy.WithOrigins("https://localhost/")
                                             .AllowAnyHeader()
                                             .AllowAnyMethod();
                      });
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseResponseCompression();
app.UseSession();

app.UseHttpsRedirection();
app.UseStaticFiles();

//啟用靜態檔案
app.UseCookiePolicy();

app.UseCors("MyAllowSpecificOrigins");


var uploadImgFolderPath = Path.Combine(app.Environment.ContentRootPath, "Image");
if (!Directory.Exists(uploadImgFolderPath))
{
    Directory.CreateDirectory(uploadImgFolderPath);
}

//var uploadFileFolderPath = Path.Combine(app.Environment.ContentRootPath, "UploadFile");
//if (!Directory.Exists(uploadFileFolderPath))
//{
//    Directory.CreateDirectory(uploadFileFolderPath);
//}


#region[讀取靜態目錄]
//讀取靜態檔案
app.UseStaticFiles(new StaticFileOptions()
{
    FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "Image")),
    RequestPath = "/Image",
});

//讀取靜態檔案
//app.UseStaticFiles(new StaticFileOptions()
//{
//    FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "File")),
//    RequestPath = "/File",
//});
#endregion

app.UseRouting();


//Use Authorization
app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
