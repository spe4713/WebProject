﻿using Microsoft.AspNetCore.Mvc;
using System.Web;
using Newtonsoft.Json;
using WebProject.InputModel;
using WebProject.Filter;
using Microsoft.AspNetCore.Authorization;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class RegisterController : Controller
    {
        private readonly UserService helper;
        public RegisterController(UserService _helper)
        {
            helper = _helper;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewUserInfo([FromForm]RegisterIM user)
        {
            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.UserName) && !string.IsNullOrEmpty(user.Email) && !string.IsNullOrEmpty(user.Password) && user.Birthday != null)
                {
                    var result = await helper.CreateUser(user.UserName, user.Email, user.Password, user.Birthday);
                    if (string.IsNullOrEmpty(result)) TempData["ErrorMsg"] = "Success";
                    else TempData["ErrorMsg"] = result;
                }
                else TempData["ErrorMsg"] = "輸入格式有缺";
            }
            else TempData["ErrorMsg"] = "請輸入註冊資料";
            return RedirectToAction("Index");
        }
    }
}
