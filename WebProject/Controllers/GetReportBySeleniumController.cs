﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NuGet.Packaging;
using NuGet.Versioning;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using System.Reflection.Emit;
using System.Text.RegularExpressions;
using WebProject.ViewModel;
using WebProject.Filter;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class GetReportBySeleniumController : Controller
    {
        private readonly GameService helper;
        public ChromeDriver chormeDriver;
        public ChromeDriverService chromeDriverService;
        public GetReportBySeleniumController(GameService _helper)
        {
            helper = _helper;
        }

        public async Task<IActionResult> Index()
        {
            string ChormDriverPath = Path.Combine(Directory.GetCurrentDirectory(), "ChormeDriver", "chromedriver.exe");
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(ChormDriverPath);
            ChromeDriver driver = new ChromeDriver(service);
            List<SpiderVM>spider= await helper.GetSpider(driver);
            if(spider.Count > 0) ViewBag.SpiderInfo = spider;
            return View();
        }
    }
}
