﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebProject.Common;
using WebProject.DAL;
using WebProject.ViewModel;

namespace WebProject.Controllers
{
    [Authorize]
    public class UserManagerController : Controller
    {
        private readonly IHttpContextAccessor context;
        public UserManagerController(IHttpContextAccessor _context)
        {

            context = _context; 

        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetUserMenu()
        {
            return Json(Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo")?.allowSystem);
        }
    }
}
