﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebProject.DAL;
using WebProject.InputModel;
using WebProject.Models;
using WebProject.ViewModel;
using WebProject.Filter;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class OrderManagerController : Controller
    {
        private readonly OrderService helper;
        public OrderManagerController(OrderService _helper)
        {
            helper = _helper;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetOrderDetail([FromForm]int EmpId,string str)
        {
            List<OrderInfoVM> orderInfos = await helper.GetOrderInfoByEmpId(EmpId,str);
            if (orderInfos.Count > 0) return Ok(orderInfos);
            return BadRequest("No Data");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateProduct([FromForm]List<ProductIM> filterCondition)
        {
            if (filterCondition.Count == 0) return BadRequest("Input Data Error");
            string result = await helper.UpdateProduct(filterCondition);
            if (string.IsNullOrEmpty(result)) return Ok("Update Success");
            return BadRequest(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteProduct([FromForm]DeleteProductIM filterCondition)
        {
            if (filterCondition == null) return BadRequest("Input Data Error");
            string result=await helper.DeleteProcuctInfo(filterCondition);
            if (string.IsNullOrEmpty(result)) return Ok("Delete Success");
            return BadRequest(result);  
        }
    }
}
