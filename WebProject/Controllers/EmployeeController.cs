﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebProject.BLL;
using WebProject.DAL_EF;
using WebProject.InputModel;
using WebProject.Filter;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class EmployeeController : Controller
    {
        private readonly EmployeeService EmpService;
        private readonly TerritoriesService TerritoriesService;
        public EmployeeController(EmployeeService _empService,TerritoriesService _territoriesService)
        {
             EmpService = _empService;
             TerritoriesService = _territoriesService;   
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetEmployeeInfo(int Id)
        {
            var empInfo =await EmpService.GetEmployeeInfo(Id);
            if (empInfo.Count > 0) return Ok(empInfo);
            return BadRequest("No Data");
        }

        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetRegion()
        {
            var result =await EmpService.GetDplRegion();
            if(result.Count>0) return Ok(result);
            return BadRequest("No Data");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ModifyTerritory([FromForm]TerritoryIM territoryCondition)
        {
            if(territoryCondition.EmployeeID==null || territoryCondition.TerritoryIds.Length==0) return BadRequest("No Data");
            var result = await TerritoriesService.UpdateTerritory(territoryCondition);
            if (string.IsNullOrEmpty(result)) return Ok("Success");
            return BadRequest("Fail");
        }
    }
}
