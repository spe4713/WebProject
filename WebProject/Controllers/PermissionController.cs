﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebProject.Common;
using WebProject.DAL;
using WebProject.InputModel;
using WebProject.Models;
using WebProject.ViewModel;
using WebProject.Filter;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class PermissionController : Controller
    {
        private readonly UserService userHelper;
        private readonly PermissionService permissionHelper;
        private readonly IHttpContextAccessor context;
        public PermissionController(UserService _userHelper, PermissionService _permissionHelper,IHttpContextAccessor _context)
        {
            userHelper = _userHelper;
            permissionHelper = _permissionHelper;
            context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> GetUserInfo()
        {
            List<UserInfoVM> user=await userHelper.GetUserInfo();   
            if(user.Count>0) return Ok(user);
            return BadRequest("No Data");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserRoleList([FromForm]string email)
        {
            List<PermissionVM> permissionInfo = await permissionHelper.GetPermissionInfo(email);
            if (permissionInfo.Count > 0) return Ok(permissionInfo);
            return BadRequest("No Data");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetUserPermission([FromForm] PermissionIM AuthorityCondition)
        {
            if (AuthorityCondition.Role == "") return BadRequest("Please select user permissions");
            string Name = Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo") == null ? "" : Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo").Name;
            string result =await permissionHelper.SetUserAuthority(AuthorityCondition, Name);
            if (string.IsNullOrEmpty(result)) return Ok("Set User Authorization Success");
            return BadRequest(result);  
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DelUserPermission([FromForm] PermissionIM AuthorityCondition)
        {
            string result = await permissionHelper.DelUserAuthority(AuthorityCondition);
            if (string.IsNullOrEmpty(result)) return Ok("Delete User Authorization Success");
            return BadRequest(result);
        }
    }
}
