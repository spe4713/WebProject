﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using WebProject.DAL;
using WebProject.Models;
using WebProject.Common;
using WebProject.InputModel;
using WebProject.BLL;

namespace WebProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserService helper;
        private readonly IHttpContextAccessor context;
        private readonly PermissionService permissionHelper;

        public HomeController(UserService _helper,IHttpContextAccessor _context, PermissionService _permissionHelper)
        {
            helper = _helper;
            context = _context;
            permissionHelper = _permissionHelper;   
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserLogin([FromForm] LoginIM UserInfo)
        {
            if (!string.IsNullOrEmpty(UserInfo.UserEmail) && !string.IsNullOrEmpty(UserInfo.Password))
            {
                var result = await helper.UserLogin(UserInfo);
                if (result != null)
                {
                    result.allowSystem =await permissionHelper.GetUserAllowSystem(result.Email);
                    var claims = new List<Claim> { new Claim(ClaimTypes.Name, result.Name) };
                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), new AuthenticationProperties
                    {
                        AllowRefresh = true,
                        IsPersistent = false,
                        RedirectUri = "/Home/Index"
                    });
                    Base.Set(context.HttpContext.Session, "UserInfo",result);
                    return Ok("/UserManager/Index");
                }
                return BadRequest("Not Find User");
            }
            return BadRequest("Please enter account and password");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            context.HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}