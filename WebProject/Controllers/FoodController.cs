﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebProject.BLL;
using WebProject.Filter;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class FoodController : Controller
    {
        private readonly DeliciousService helper;
        public FoodController(DeliciousService _helper)
        {
            helper = _helper;
        }
        public  IActionResult Index()
        {
            return View();
        }

        //美食地圖 資料量大不跟location放成一個ViewModel
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetFoodInfo()
        {
            var info =await helper.GetTaiwanDelicios();
            if (info.Count > 0) return Ok(info);
            return BadRequest("No Data");
        }

        //get taiwan city
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetCity()
        {
            var result=await helper.GetTaiwanCity();   
            if(result!=null) return Ok(result);
            return BadRequest("No Data");
        }
    }
}
