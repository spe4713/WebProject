﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebProject.DAL;
using WebProject.ViewModel;
using WebProject.Filter;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    [ActionFilter]
    public class OrderController : Controller
    {
        private readonly SalesService SalesHelper;
        private readonly OrderService OrderHelper;
        public OrderController(SalesService _SaelsHelper, OrderService _OrderHelper)
        {
            SalesHelper = _SaelsHelper;
            OrderHelper = _OrderHelper;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetSales()
        {
            List<SelectListItem> salesList =await SalesHelper.GetEmployee();
            if(salesList.Count>0) return Ok(salesList);
            return BadRequest("No Data");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetOrderBySales([FromForm]int SalesId)
        {
            List<OrderVM> info = await OrderHelper.GetOrderBySales(SalesId);
            if (info.Count > 0) return Ok(info);
            return BadRequest("No Data");
        }
    }
}
