﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebProject.Common;
using WebProject.InputModel;
using WebProject.Models;
using WebProject.ViewModel;
using WebProject.BLL;

namespace WebProject.Controllers
{
    [Authorize]
    public class ChangePwdController : Controller
    {
        private readonly UserService helper;
        private readonly IHttpContextAccessor context;
        public ChangePwdController(UserService _helper,IHttpContextAccessor _context)
        {
            helper = _helper;
            context = _context; 
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdatePassword([FromForm] ModifyPwdInfoIM ModifyCondition)
        {
            string Email = Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo")==null?"": Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo").Email;
            string result = await helper.UpdateUserPwd(Email, ModifyCondition.NewPassword, ModifyCondition.OldPassword);
            if (string.IsNullOrEmpty(result)) return Ok("密碼修改成功");
            else return BadRequest(result); 
        }
    }
}
