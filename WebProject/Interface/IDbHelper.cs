﻿using Dapper;

namespace WebProject.Interface
{
    public interface IDbHelper
    {
        public Task<T> GetSingleAsync<T>(string command, object parms);
        public Task<List<T>> GetAllAsync<T>(string command, object parms);
        public Task<string> ExecuteAsync(string command, object parms);
        public Task<string> ExecuteMutiAsync<T>(string command, List<T> parms);
    }
}
