﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace WebProject.Common
{
    public  class Base
    {
        public static void Set<T>(ISession session,string key,T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(ISession session,string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonConvert.DeserializeObject<T>(value);
        }

        public  List<SelectListItem> RoleType = new List<SelectListItem>
        {
           new SelectListItem{Text="==請選擇==",Value="" },
            new SelectListItem{Text="Admin",Value="10" },
            new SelectListItem{Text="Editor",Value="20"},
            new SelectListItem{Text="Reader",Value="30"},
        };
    }
}
