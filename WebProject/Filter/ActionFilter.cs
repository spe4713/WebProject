﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebProject.Common;
using WebProject.ViewModel;

namespace WebProject.Filter
{
    public class ActionFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var UserInfo = Base.Get<UserInfoVM>(context.HttpContext.Session, "UserInfo");
            if (UserInfo != null && UserInfo.allowSystem.Count > 0)
            {
                var IsExist = false;
                var controller = context.ActionDescriptor.RouteValues["controller"];
                var action = context.ActionDescriptor.RouteValues["action"];
                if (controller != null && action != null)
                {
                    IsExist = UserInfo.allowSystem.Any(m => m.Url.Contains(controller));
                }
                string firstPage = UserInfo.allowSystem.Count > 0 ? $"~/{UserInfo.allowSystem[0].Url}" : "~/UserManager/Index";
                if (!IsExist)
                {
                    context.Result = new RedirectResult(firstPage);
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }
    }
}
