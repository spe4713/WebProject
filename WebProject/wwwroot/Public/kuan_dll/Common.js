function SweetAlert(type, text, timer) {
    Swal.fire({
        icon: type,
        text: text,
        timer: timer
    })
}

function CheckEmail(value) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(value)) return false;
    else return true;
    }

//包含數字與英文
function CheckPassword(pwd) {
    if (pwd == null || pwd.length < 6) return false;
    var regu = "((^[a-zA-Z]{1,}[0-9]{1,}[a-zA-Z0-9]*)+)|((^[0-9]{1,}[a-zA-Z]{1,}[a-zA-Z0-9]*)+)$"
    var reg = new RegExp(regu);
    if (reg.test(pwd)) {
        return true;
    } else {
        return false;
    }
}



function ComparePwd() {
    if ($("#NewPassword").val() != $("#ConfirmPassword").val()) { return false; }
    else { return true; }
}


function ChkEmail() {
    if (CheckEmail($("#Email").val())) {
        $("#erroreMail").text("")
        return false;
    } else {
        $("#erroreMail").text("信箱格式有誤");
        return true;
    }
}

function IsNotEmpty(value) {
    if (value.trim() == "") {
        return false;
    }
    return true;
}

function SetMenu(data) {
    Cookies.set('Menu', data, { expires: 1, sameSite: 'strict' })
}

function GetMenu() {
    return Cookies.get('Menu');
}