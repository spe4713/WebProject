﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using WebProject.Models;

namespace WebProject.DAL_EF
{
    public class EmployeeTerritoriesRepository
    {

        private NorthWindContext Context;
        private readonly ILogger<EmployeeTerritoriesRepository> logger;

        public EmployeeTerritoriesRepository(NorthWindContext _context, ILogger<EmployeeTerritoriesRepository> _logger)
        {
            Context = _context;
            logger = _logger;
        }

        public void SetDbContextForRelation(NorthWindContext context)
        {
            Context = context;
        }

        public IQueryable<EmployeeTerritories> GetEmployeeTerritories(Expression<Func<EmployeeTerritories, bool>> funcWhere)
        {
            return Context.EmployeeTerritories.Where(funcWhere);
        }

        public IQueryable<EmployeeTerritories> GetEmployeeTerritoriesOfSql(string sql)
        {
            return Context.EmployeeTerritories.FromSqlRaw(sql);
        }


        public string BatchCreateEmployeeTerritories(List<EmployeeTerritories> EmployeeTerritoriesInfos)
        {
            if (EmployeeTerritoriesInfos.Count == 0) return "EmployeeTerritories is null";

            try
            {
                Context.EmployeeTerritories.AddRange(EmployeeTerritoriesInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchUpdateEmployeeTerritories(List<EmployeeTerritories> EmployeeInfos)
        {
            try
            {
                Context.EmployeeTerritories.UpdateRange(EmployeeInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string UpdateEmployeeTerritories(EmployeeTerritories EmployeeTerritoriesInfo)
        {
            try
            {
                Context.EmployeeTerritories.Update(EmployeeTerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string DeleteEmployeeTerritories(EmployeeTerritories EmployeeTerritoriesInfo)
        {
            try
            {
                Context.EmployeeTerritories.Remove(EmployeeTerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchDeleteEmployeeTerritories(List<EmployeeTerritories> EmployeeTerritoriesInfo)
        {
            try
            {
                Context.EmployeeTerritories.RemoveRange(EmployeeTerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public void Dispose()
        {
            Context?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
