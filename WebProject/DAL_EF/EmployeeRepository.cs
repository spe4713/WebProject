﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using WebProject.Models;

namespace WebProject.DAL_EF
{
    public class EmployeeRepository
    {
        private NorthWindContext Context;
        private readonly ILogger<EmployeeRepository> logger;

        public EmployeeRepository(NorthWindContext _context, ILogger<EmployeeRepository> _logger)
        {
            Context = _context;
            logger = _logger;
        }

        public void SetDbContextForRelation(NorthWindContext context)
        {
            Context = context;
        }

        public IQueryable<Employees> GetEmployee(Expression<Func<Employees, bool>> funcWhere)
        {
             return Context.Employees.Where(funcWhere);
        }

        public IQueryable<Employees> GetEmployeeOfSql(string sql)
        {
            return Context.Employees.FromSqlRaw(sql);
        }


        public string BatchCreateEmployee(List<Employees> EmployeeInfos)
        {
            if (EmployeeInfos.Count == 0) return "Employee is null";

            try
            {
                Context.Employees.AddRange(EmployeeInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchUpdateEmployeeInfos(List<Employees> EmployeeInfos)
        {
            try
            {
                Context.Employees.UpdateRange(EmployeeInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string UpdateEmployee(Employees EmployeeInfo)
        {
            try
            {
                Context.Employees.Update(EmployeeInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string DeleteEmployee(Employees EmployeeInfo)
        {
            try
            {
                Context.Employees.Remove(EmployeeInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchDeleteEmployee(List<Employees> EmployeeInfo)
        {
            try
            {
                Context.Employees.RemoveRange(EmployeeInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public void Dispose()
        {
            Context?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
