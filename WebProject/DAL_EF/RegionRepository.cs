﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using WebProject.Models;

namespace WebProject.DAL_EF
{
    public class RegionRepository
    {
        private NorthWindContext Context;
        private readonly ILogger<RegionRepository> logger;

        public RegionRepository(NorthWindContext _context, ILogger<RegionRepository> _logger)
        {
            Context = _context;
            logger = _logger;
        }

        public void SetDbContextForRelation(NorthWindContext context)
        {
            Context = context;
        }

        public IQueryable<Region> GetRegion(Expression<Func<Region, bool>> funcWhere)
        {
            return Context.Region.Where(funcWhere);
        }

        public IQueryable<Region> GetTerritoriesOfSql(string sql)
        {
            return Context.Region.FromSqlRaw(sql);
        }


        public string BatchCreateRegion(List<Region> RegionInfos)
        {
            if (RegionInfos.Count == 0) return "Region is null";

            try
            {
                Context.Region.AddRange(RegionInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchUpdateRegion(List<Region> RegionInfos)
        {
            try
            {
                Context.Region.UpdateRange(RegionInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string UpdateRegion(Region RegionInfo)
        {
            try
            {
                Context.Region.Update(RegionInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string DeleteRegion(Region RegionInfo)
        {
            try
            {
                Context.Region.Remove(RegionInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchDeleteRegion(List<Region> RegionInfo)
        {
            try
            {
                Context.Region.RemoveRange(RegionInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public void Dispose()
        {
            Context?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
