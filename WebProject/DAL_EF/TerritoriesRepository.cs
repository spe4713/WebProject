﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using WebProject.Models;

namespace WebProject.DAL_EF
{
    public class TerritoriesRepository
    {

        private NorthWindContext Context;
        private readonly ILogger<TerritoriesRepository> logger;

        public TerritoriesRepository(NorthWindContext context, ILogger<TerritoriesRepository> _logger)
        {
            this.Context = context;
            logger = _logger;
        }

        public void SetDbContextForRelation(NorthWindContext context)
        {
            this.Context = context;
        }

        public IQueryable<Territories> GetTerritories(Expression<Func<Territories, bool>> funcWhere)
        {
            return Context.Territories.Where(funcWhere);
        }

        public IQueryable<Territories> GetTerritoriesOfSql(string sql)
        {
            return Context.Territories.FromSqlRaw(sql);
        }


        public string BatchCreateTerritories(List<Territories> TerritoriesInfos)
        {
            if (TerritoriesInfos.Count == 0) return "Territories is null";

            try
            {
                Context.Territories.AddRange(TerritoriesInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchUpdateTerritories(List<Territories> TerritoriesInfos)
        {
            try
            {
                Context.Territories.UpdateRange(TerritoriesInfos);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string UpdateTerritories(Territories TerritoriesInfo)
        {
            try
            {
                Context.Territories.Update(TerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string DeleteTerritories(Territories TerritoriesInfo)
        {
            try
            {
                Context.Territories.Remove(TerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public string BatchDeleteTerritories(List<Territories> TerritoriesInfo)
        {
            try
            {
                Context.Territories.RemoveRange(TerritoriesInfo);
                Context.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                logger.LogError($"Error:{e.Message}");
                return e.Message;
            }
        }

        public void Dispose()
        {
            Context?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
